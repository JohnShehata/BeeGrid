﻿var BeeGrid = function () {
    var Obj = {
        Params: {},
        DefaultSortIcon: 'glyphicon-sort',
        AscSortIcon: 'glyphicon-sort-by-attributes',
        DescSortIcon: 'glyphicon-sort-by-attributes-alt',
        SortProp: null,
        SortDirection: null,
        PagerHolderID: null,
        GridID: null,
        Url: null,
        PageSize: null,
        TotalPages: null,
        CurrentPage: function () {
            return $('#' + this.PagerHolderID + ' ul.pagination li.active').data('val');
        },
        ChangePageEvent: function () {
            var ThisObj = this;
            this.Params.CurrentPage = ThisObj.CurrentPage()
            $.ajax({
                url: this.Url,
                type: 'POST',
                dataType: "JSON",
                data: this.Params,
                success: function (Result) {
                    ThisObj.TableBind(Result.data);
                }
            });
        },
        TableBind: function (Result) {
            var BodyObj = $('#' + this.GridID + '>tbody')
            var Ths = $('#' + this.GridID + '>thead>tr>th');
            $(BodyObj).html('');
            var Tr = null;
            for (var i = 0; i < Result.length; i++) {
                Tr = $('<tr></tr>');
                var Item = Result[i];
                for (var j = 0; j < $(Ths).length; j++) {
                    $(Tr).append('<td>' + Item[$(Ths[j]).data('propname')] + '</td>');

                }
                $(BodyObj).append(Tr);
            }
        },
        LastPaging: function () {
            if (this.TotalPages > 5)
                return 5
            else
                return this.TotalPages;
        },
        GetFirstPage: function () {
            return $($('#' + this.PagerHolderID + ' ul.pagination li.nm').first()).data('val');
        },
        GetLastPage: function () {
            return $($('#' + this.PagerHolderID + ' ul.pagination li.nm').last()).data('val');
        },
        CreatePager: function () {
            var UiObj = $('<ul class="pagination"></ul>');
            $(UiObj).append('<li data-val="prev" class="paginate_button previous"><a href=""><span class="glyphicon glyphicon-backward"></span></a></li>')
            $(UiObj).append('<li data-val="prev" class="paginate_button first"><a href=""><span class="glyphicon glyphicon-fast-backward"></span></a></li>')

            for (var i = 1; i <= this.LastPaging() ; i++) {
                $(UiObj).append('<li data-val=' + i.toString() + ' class="paginate_button nm"><a href="">' + i.toString() + '</a></li>');
            }
            $(UiObj).append('<li data-val="prev" class="paginate_button last"><a href=""><span class="glyphicon glyphicon-fast-forward"></span></a></li>')
            $(UiObj).append('<li data-val="nxt" class="paginate_button next"><a href=""><span class="glyphicon glyphicon-forward"></span></a></li>')
            $('#' + this.PagerHolderID).html('');
            $('#' + this.PagerHolderID).append(UiObj);
            this.SetActivePage(1);
        },
        GoPage: function (Page) {

            if (Page < this.GetFirstPage()) {
                var Distance = this.GetFirstPage() - Page;
                this.SHIFT('right', Distance)
                this.SetActivePage(Page);
            }
            else if (Page > this.GetLastPage()) {
                var Distance = Page - this.GetLastPage();
                this.SHIFT('left', Distance)
                this.SetActivePage(Page);
            }
            else
                this.SetActivePage(Page);

        },
        GridHeaderClick: function () {
            var ThisObj = this;
            $('#' + ThisObj.GridID + ' th.sort').click(function () {
                var SortSpn = $(this).find('span.glyphicon');
                $('#' + ThisObj.GridID).find('span.'+ThisObj.AscSortIcon).removeClass(ThisObj.AscSortIcon).addClass(ThisObj.DefaultSortIcon);
                $('#' + ThisObj.GridID).find('span.'+ThisObj.DescSortIcon).removeClass(ThisObj.DescSortIcon).addClass(ThisObj.DefaultSortIcon);


                if ($(this).data('propname').toLowerCase() == ThisObj.Params.SortProp.toLowerCase()) {
                    if (ThisObj.Params.SortDirection.toLowerCase() == "asc") {
                        ThisObj.Params.SortDirection = "desc";
                        $(SortSpn).removeClass(ThisObj.DefaultSortIcon)
                        $(SortSpn).addClass(ThisObj.DescSortIcon)
                    }
                    else {
                        ThisObj.Params.SortDirection = "asc";
                        $(SortSpn).removeClass(ThisObj.DefaultSortIcon)
                        $(SortSpn).addClass(ThisObj.AscSortIcon)
                    }
                }
                else {
                    ThisObj.Params.SortDirection = "asc";
                    ThisObj.Params.SortProp = $(this).data('propname');
                    $(SortSpn).addClass(ThisObj.AscSortIcon);
                }
                ThisObj.Params.CurrentPage = 1;
                $.ajax({
                    url: ThisObj.Url,
                    type: 'POST',
                    dataType: "JSON",
                    data:ThisObj.Params,
                    //data: {
                    //    PageSize: ThisObj.PageSize,
                    //    SortProp: ThisObj.SortProp,
                    //    SortDirection: ThisObj.SortDirection,


                    //},
                    success: function (Result) {
                        ThisObj.GoPage(1);
                        ThisObj.TableBind(Result.data);
                    }
                });
            })
        },
        FirstPageClick: function () {
            var ThisCls = this;
            $(document).on('click', '#' + this.PagerHolderID + ' .paginate_button.first', function () {
                ThisCls.GoPage(1);
                ThisCls.ChangePageEvent();
            })
        },
        LastPageClick: function () {
            var ThisCls = this;
            $(document).on('click', '#' + this.PagerHolderID + ' .paginate_button.last', function () {
                ThisCls.GoPage(ThisCls.TotalPages);
                ThisCls.ChangePageEvent();

            })
        },
        DisableJump: function () {
            $(document).on('click', '#' + this.PagerHolderID + ' ul.pagination a', function (event) {
                event.preventDefault();
                //return false;
            })
           
        },
        SHIFT: function (direction, steps) {
            var PagersList = $('#' + this.PagerHolderID + ' ul.pagination li.nm');
            var FirstPage = this.GetFirstPage();
            var LastPage = this.GetLastPage();

            var NewFirstPage = 0;
            var NewLastPage = 0;

            if (direction == "left") {
                NewFirstPage = FirstPage + steps;
                NewLastPage = LastPage + steps;
            }
            else if (direction == "right") {
                NewFirstPage = FirstPage - steps;
                NewLastPage = LastPage - steps;
            }
            var Index = 0;
            var lastBtn = $('#' + this.PagerHolderID + ' .paginate_button.last');
            $('#' + this.PagerHolderID + ' ul.pagination li.nm').remove();
            for (var i = NewFirstPage; i <= NewLastPage; i++) {
                $('<li data-val=' + i.toString() + ' class="paginate_button nm"><a href="#">' + i.toString() + '</a></li>').insertBefore($(lastBtn));
            }
        },
        MovePrev: function () {
            var CurrentPage = this.CurrentPage();
            this.SetActivePage(parseInt(CurrentPage) - 1);
        },
        DisablePrevButton: function () {
            if (!$('#' + this.PagerHolderID + ' .paginate_button.previous').hasClass('disabled'))
                $('#' + this.PagerHolderID + ' .paginate_button.previous').addClass('disabled');
        },
        DisableNextButton: function () {
            if (!$('#' + this.PagerHolderID + ' .paginate_button.next').hasClass('disabled'))
                $('#' + this.PagerHolderID + ' .paginate_button.next').addClass('disabled');
        },
        EnablePrevButton: function () {
            $('#' + this.PagerHolderID + ' .paginate_button.previous').removeClass('disabled');
        },
        EnableNextButton: function () {
            $('#' + this.PagerHolderID + ' .paginate_button.next').removeClass('disabled');
        },
        MoveNext: function () {
            var CurrentPage = this.CurrentPage();
            this.SetActivePage(parseInt(CurrentPage) + 1);
        },
        ManageEnableDisablePrevNextButtons: function () {
            this.EnableNextButton();
            this.EnablePrevButton();

            if (this.CurrentPage() == 1)
                this.DisablePrevButton();
            else if (this.CurrentPage() == this.TotalPages)
                this.DisableNextButton();
        },
        SetActivePage: function (Page) {
            if (Page >= 1 && Page <= this.TotalPages && this.CurrentPage() != Page) {
                if (Page >= this.GetFirstPage() && Page <= this.GetLastPage()) {
                    this.MarkPage(Page);
                }
                else if (Page < this.GetFirstPage()) {
                    this.SHIFT('right', 1);
                    this.MarkPage(Page);
                }
                else if (Page > this.GetLastPage()) {
                    this.SHIFT('left', 1);
                    this.MarkPage(Page);
                }
                this.ManageEnableDisablePrevNextButtons();
                //this.ChangePageEvent();
            }
        },
        MarkPage: function (Page) {
            $('#' + this.PagerHolderID + ' ul.pagination li.nm').removeClass('active');
            $('#' + this.PagerHolderID + ' ul.pagination li.nm[data-val="' + Page + '"]').addClass('active');
        },
        PrevButtonClick: function () {
            var ThisCls = this;
            $('#' + this.PagerHolderID + ' .paginate_button.previous').click(function () {
                ThisCls.MovePrev();
                ThisCls.ChangePageEvent();
            })
        },
        NextButtonClick: function () {
            var ThisCls = this;
            $(document).on('click', '#' + this.PagerHolderID + ' .paginate_button.next', function () {
                ThisCls.MoveNext();
                ThisCls.ChangePageEvent();

            })
        },
        PageButtonsClick: function () {
            var ThisCls = this;
            $(document).on('click', '#' + this.PagerHolderID + ' ul.pagination li.nm', function () {
                ThisCls.SetActivePage($(this).data('val'));
                ThisCls.ChangePageEvent();

            })

        },
        Update: function () {
            var ThisCls = this;
            this.CreatePager();
        },
        Initiate: function () {
            this.CreatePager();
            this.DisableJump();
            this.PageButtonsClick();
            this.PrevButtonClick();
            this.NextButtonClick();
            this.FirstPageClick();
            this.LastPageClick();
            this.GridHeaderClick();
            //this.ChangePageEvent();
        },
        Call: function () {
            var ThisObj = this;
            this.Params.CurrentPage = 1;
            $.ajax({
                url: this.Url,
                type: 'POST',
                dataType: "JSON",
                data: this.Params,
                success: function (Result) {
                    ThisObj.TotalPages = Result.TotalPages;
                    ThisObj.Initiate();
                    ThisObj.TableBind(Result.data);
                }
            });
        },
        Search: function () {
            var ThisObj = this;
            this.Params.CurrentPage = 1;
            $.ajax({
                url: this.Url,
                type: 'POST',
                dataType: "JSON",
                data: this.Params,
                success: function (Result) {
                    ThisObj.TotalPages = Result.TotalPages;
                    ThisObj.Update();
                    ThisObj.TableBind(Result.data);
                }
            });
        },

    }
    return Obj;
}